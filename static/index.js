$(function () {
   
    $('[data-toggle="popover-hover"]').popover({
        html: true,
        trigger: 'hover',
        placement: 'bottom',
        content: function () { return '<img src="' + $(this).data('img') + '" />'; }
        });

    $("img").on("error", function() {
        console.log("img error");
        $(this).hide();
    });

    $.ajax({
        url: '/temperature',
        type: 'GET',
        success: function(data) {
            $('#mintemp_val').html( data.minTempRange + '&#8451;');
            $('#maxtemp_val').html( data.maxTempRange + '&#8451;');
            if(data.tempConfigure == 1){
                $("#mintemp_val").show();
                $("#minTempRange").show();
                $("#maxtemp_val").show();
                $("#maxTempRange").show();
                $("#mintemp_val_lbl").show();
                $("#maxtemp_val_lbl").show();
                $("#button").show();
                
            }else{
                $("#mintemp_val").hide();
                $("#minTempRange").hide();
                $("#maxtemp_val").hide();
                $("#maxTempRange").hide();
                $("#mintemp_val_lbl").hide();
                $("#maxtemp_val_lbl").hide();
                $("#button").hide();
            }
            if(data.humanPresence == 1){
                $('#avgTemp').html( data.avgTemp );
            }else{
                $('#avgTemp').html("");
            }

            if(data.faceMaskInd == 1){
                $('#maskIndicator').html( "Face mask detected" );
            }else{
                $('#maskIndicator').html(" No face mask detected");
            }
            $('#minTempRange').val(data.minTempRange);
            $('#maxTempRange').val(data.maxTempRange);
        },
        error: function(error) {
            console.log(error);
        }
    });
    
    $('button').click(function() {
        $.ajax({
            url: '/configure-temp',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

});

var startTime, endTime, seconds;

function start() {
    startTime = new Date();
};

function end() {
    endTime = new Date();
    var timeDiff = endTime - startTime; //in ms
    // strip the ms
    timeDiff /= 1000;

    // get seconds 
    seconds = Math.round(timeDiff);
    console.log(seconds + " seconds");
}

setInterval(function() {
    $.ajax({
        url: '/thermal-img',
        type: 'GET',
        success: function(data) {
            start();
            $('#image').attr('src', data);
            $('#duration').html( 'Last updated ' + seconds + ' seconds ago' );
        },
        error: function(error) {
            console.log(error);
        }
    });
    
    $.ajax({
        url: '/person-img',
        type: 'GET',
        success: function(data) {
            $('#camera').attr('src', data);
            $('#duration2').html( 'Last updated ' + seconds + ' seconds ago' );
        },
        error: function(error) {
            console.log(error);
        }
    });

    $.ajax({
        url: '/temperature',
        type: 'GET',
        success: function(data) {
            if(data.humanPresence == 1){
                $('#avgTemp').html( data.avgTemp + '&#8451;');
            }else{
                $('#avgTemp').html("");
            }
            console.log("face mask detection > " +  data.faceMaskInd);
            if(data.faceMaskInd == 1){
                $('#maskIndicator').html( "Face mask detected" );
            }else{
                $('#maskIndicator').html(" No face mask detected");
            }
            end();
        },
        error: function(error) {
            console.log(error);
        }
    });

}, 2500);

$(document).on('input change', '#minTempRange', function() {
    $('#mintemp_val').html( $(this).val() );
});
$(document).on('input change', '#maxTempRange', function() {
    $('#maxtemp_val').html( $(this).val() );
});