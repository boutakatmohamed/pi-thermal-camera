## Thermal camera sensor 8x8 (AMG8833) + Raspberry Pi 4 + Simple Web App (LANCZOS Sampling Canvas) + Pi Camera + Google Coral USB (Tensorflow Lite)

Everything is written in python.

### Required Hardware ...

- Raspberry Pi 4 4GB RAM (https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2962)
- 16GB MicroSD card (https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2987) 
- Adafruit AMG8833 Thermal Camera 8x8 (https://www.adafruit.com/product/3538)
- Jumper cable (https://www.sgbotic.com/index.php?dispatch=products.view&product_id=1910)
- Grove connector cable (http://amicus.com.sg/migration_prd/shop/grove/cable/grove-universal-4-pin-buckled-20cm-cable-5-pcs-pack/)
- Raspberry Pi Camera v2 (http://amicus.com.sg/migration_prd/shop/raspberry-pi-2/raspberry-pi-camera-v2-camera-module/)
- Google Coral USB - Google Edge TPU coprocessor(https://coral.ai/products/accelerator/)
- WS2812 Addressable RGB LED Stick (https://www.sgbotic.com/index.php?dispatch=products.view&product_id=1928)
  <br>

### 1. Burn Raspbian Lite on to MicroSD card

https://www.raspberrypi.org/documentation/installation/installing-images/README.md

### 2. Plug external monitor, keyboard and mouse to the Raspberry Pi

* Enable SSH select P2

```
$ sudo raspi-config
```

Select P2 and enable the SSH 

<img src="./images/raspi-config2.png"/>

### 3. Clone the Pi-Thermal-Camera repository from Gitlab to the Pi

On your laptop/desktop connect to the Raspberry Pi, check your router/wifi hotspot for the raspberry pi ip address.

```
ssh -v pi@<ip addrress of the raspberry pi>
```

```
$ git clone https://gitlab.com/kenken64/pi-thermal-camera.git
```

### 4. Install all the dependencies

Change to the cloned repo directory

```
$ cd pi-thermal-camera
```

Install all the required python libraries

```
$ sudo pip3 install -r requirements.txt
```

List all the installed libraries

```
$ sudo pip3 list
```


### 5. Setup and Install Raspbian (Linux) Service unit

Enable I2C bus using the below command line, select P5 and enable the feature

```
$ sudo raspi-config
```
<img src="./images/raspi-config2.png"/>


Create a new python thermal cam service using the nano editor

```
$ sudo nano /lib/systemd/system/thermal.camera.service
```

Copy and paste this configuration to the above file

```
[Unit]
Description=Thermal Camera Listener
After=multi-user.target

[Service]
Type=idle
User=pi
ExecStart=/usr/bin/python3 /home/pi/pi-thermal-camera/thermal-camera.py
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target

```

To enable the above service

```
$ sudo systemctl enable thermal.camera.service
$ sudo systemctl daemon-reload
```

To restart the above service

```
$ sudo systemctl restart thermal.camera.service
```

### 6. Create the web app has dependencies on the main thermal sensor app.

Create a new python flask service using the nano editor

```
$ sudo nano /lib/systemd/system/webapp.tc.service
```

Copy and paste this configuration to the above file

```
[Unit]
Description=Thermal Camera Web App
After=multi-user.target
Requires=thermal.camera.service
After=thermal.camera.service

[Service]
Type=idle
User=pi
ExecStart=/home/pi/pi-thermal-camera/run.sh
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target

```

To enable the above service

```
$ sudo systemctl enable webapp.tc.service
$ sudo systemctl daemon-reload
```

To restart the above service

```
$ sudo systemctl restart webapp.tc.service
```



### 7. Face tracking trigger - Temperature reading

<br>
<img src="./images/screen5.png"/>
<br>

### Configurable sensor's min and max temperature threshold ...

<br>
<img src="./images/screen4.png"/>
<br>

### Thermal image when everything is room temperature ...

<br>
<img src="./images/screen1.png"/>
<br>

### Top right getting a bit warm where heat map indicating orange

<img src="./images/screen2.png"/>
<br>

### Extremely burning hot! Top with red hot colour

<br>
<img src="./images/screen3.png"/>

### Wow face detection ... only read temperature when camera detect human face

<br>
<img src="./images/app.jpg"/>
<br>

## 8. Added face detection on the Pi camera

Enable Pi Camera using the below command line, select P1 and enable the feature

```
$ sudo raspi-config
```
<img src="./images/raspi-config3.png"/>

<img src="./images/raspi-config4.png"/>

Install Google Coral TPU Edge Processor

https://coral.ai/docs/accelerator/get-started/#on-linux

https://www.tensorflow.org/lite/guide/python

```
$ sudo apt-get update
$ sudo apt-get install python3-edgetpu
```

To test the python program that detect face, Ctrl + C/X to terminate the program

```
 $ python capture_face.py --model /home/pi/pi-thermal-camera/models/ssd_mobilenet_v2_face_quant_postprocess_edgetpu.tflite --input /home/pi/pi-thermal-camera/picamera.jpg --output /home/pi/pi-thermal-camera/result.png --fmmodel models/fm_model_quant_int8_edgetpu.tflite --fmlabel /home/pi/pi-thermal-camera/models/facemask_labels.txt 
```

Create a new python face detection service using the nano editor

```
$ sudo nano /lib/systemd/system/face.detection.service
```

Copy and paste this configuration to the above file

```
[Unit]
Description=Face detection
After=multi-user.target

[Service]
Type=idle
User=pi
ExecStart=/usr/bin/python3 /home/pi/pi-thermal-camera/capture_face.py --model /home/pi/pi-thermal-camera/models/ssd_mobilenet_v2_face_quant_postprocess_edgetpu.tflite --input /home/pi/pi-thermal-camera/picamera.jpg --output /home/pi/pi-thermal-camera/result.png --fmmodel models/fm_model_quant_int8_edgetpu.tflite --fmlabel /home/pi/pi-thermal-camera/models/facemask_labels.txt
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target

```

To enable the above service

```
$ sudo systemctl enable face.detection.service
$ sudo systemctl daemon-reload

```

To restart the face detection service

```
$ sudo systemctl restart face.detection.service
```

## 9. Service for Ngrok tunneling 

Make sure you have an account with ngrok , download the arm ngrok binaries, unzip the archieve into the /home/pi directory.
Then register the auth token within the raspberry pi according to their instructions

Create a new ngrok tunnel service using the nano editor

```
$ sudo nano /lib/systemd/system/ngrok.service
```

```
[Unit]
Description=Ngrok tunnel
After=multi-user.target webapp.tc.service
Requires=webapp.tc.service

[Service]
Type=idle
User=pi
ExecStart=/home/pi/ngrok http 5000 --region=ap --subdomain=thermalcam
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target

```

To enable the above service

```
$ sudo systemctl enable ngrok.service
$ sudo systemctl daemon-reload

```

Restart ngrok tunnel

```
 sudo systemctl restart ngrok.service
```

## 10. Setup the Light up RGB LED strip 

Enable SPI bus using the below command line, select P4 and enable the feature

```
$ sudo raspi-config
```
<img src="./images/raspi-config2.png"/>

Create a new python blink fever service using the nano editor

```
$ sudo nano /lib/systemd/system/blink.fever.service
```

Copy and paste this configuration to the above file

```
[Unit]
Description=RGB LED strip fever blink blink
After=multi-user.target
After=multi-user.target thermal.camera.service
Requires=thermal.camera.service

[Service]
Type=idle
User=pi
ExecStart=sudo /usr/bin/python3 /home/pi/pi-thermal-camera/blinkfever.py
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target
```

To enable the above service

```
$ sudo systemctl enable blink.fever.service
$ sudo systemctl daemon-reload

```

```
 sudo systemctl restart blink.fever.service
```

## Device's Rest API


### configure-temp

- Description: This endpoint is used to configure the min and max threshold of the thermal cam device.
- HTTP Method : POST 
- URI End point: ```<api url>```/configure-temp
- JSON Output :
```
{
  'minTempRange': 37.5,
  'maxTempRange': 39
}
```

### temperature

- Description: This endpoint is used to retrieve al the data is required to determine the person is standing in front of the device, temperature from the sensor, whether he/she is wearing a mask.
- HTTP Method : GET 
- URI End point: ```<api url>```/temperature
- JSON Output :
```
{
    'minTempRange': 37.5,
    'maxTempRange': 39,
    'avgTemp': 36.9,
    "humanPresence": 1,
    "tempConfigure": 1,
    "faceMaskInd": 1
  }
```

### thermal-img

- Description: Return a thermal image from the device.
- HTTP Method : GET 
- URI End point: ```<api url>```/thermal-img
- Output : Base64 Image


### person-img

- Description: Return a person image with face and mask detection bounding box from the device.
- HTTP Method : GET 
- URI End point: ```<api url>```/person-img
- Output : Base64 Image

<div style="width:100%; height:100%">
  <img src="./images/hardware5.jpg"/>
  <img src="./images/hardware1.jpg" />
  <img src="./images/hardware2.jpg" />
  <img src="./images/hardware3.jpg" />
  <br>
  <img src="./images/hardware4.jpg" />
</div>
