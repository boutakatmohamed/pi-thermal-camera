from dotenv import load_dotenv
load_dotenv(verbose=True)

import board
import neopixel
import os
import time


# GPIO pin 18 with 8 addressable leds
pixels = neopixel.NeoPixel(board.D18, 8, brightness=0.5, auto_write=False)

# initialize all the env variables
BLINK_FEVER_FILE = os.getenv("BLINK_FEVER_FILE")
PROJ_DIR = os.getenv("PROJ_DIR")

# control the led strip off all the individual leds
def lightsOff():
    pixels.fill((0, 0, 0))

def feverLighUp():
    pixels.fill((0, 255, 0))

def nofeverLighUp():
    pixels.fill((255, 0, 0))

while True:
    file = open(PROJ_DIR + BLINK_FEVER_FILE, "r")
    value = file.read()
    print(value)
    time.sleep(2)

    if value == '0':
        lightsOff()

    if value == '1':
        print("The dude standing in front of the device having FEVER!")
        feverLighUp()

    if value == '2':
        print("You are OK go ahead enter the premise !")
        nofeverLighUp()
    pixels.show()
