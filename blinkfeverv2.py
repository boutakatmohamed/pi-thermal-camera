from dotenv import load_dotenv
load_dotenv(verbose=True)

import board
#import neopixel
import os
import time
from apa102 import APA102

PIXELS_N = 12
pixels = APA102(num_led=PIXELS_N)

# initialize all the env variables
BLINK_FEVER_FILE = os.getenv("BLINK_FEVER_FILE")
PROJ_DIR = os.getenv("PROJ_DIR")

# control the led strip off all the individual leds
def lightsOff():
    fill((0, 0, 0))

# control the led strip turn it to red all the individual leds
def feverLighUp():
    fill((255, 0, 0))

# control the led strip turn it to green all the individual leds
def nofeverLighUp():
    fill((0, 255, 0))

def fill(rgb):
    for led in range(PIXELS_N):
      pixels.set_pixel(led, rgb[0], rgb[1], rgb[2])


while True:
    file = open(PROJ_DIR + BLINK_FEVER_FILE, "r")
    value = file.read()
    print(value)
    time.sleep(2)
    if value == '0':
        print('light off')
        lightsOff()

    if value == '1':
        print("The dude standing in front of the device having FEVER!")
        feverLighUp()

    if value == '2':
        print("You are OK go ahead enter the premise !")
        nofeverLighUp()
    pixels.show()
