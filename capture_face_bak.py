from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from dotenv import load_dotenv
load_dotenv(verbose=True)

import argparse
import io
import os
import time
import numpy as np
import picamera

from PIL import Image

from edgetpu.detection.engine import DetectionEngine
from edgetpu.utils import dataset_utils
from PIL import ImageDraw

HUMAN_PRESENCE_FILE = os.getenv("HUMAN_PRESENCE_FILE")
PROJ_DIR = os.getenv("PROJ_DIR")

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--model', required=True,
      help='Detection SSD model path (must have post-processing operator).')
  parser.add_argument('--label', help='Labels file path.')
  parser.add_argument('--input', help='Input image path.', required=True)
  parser.add_argument('--output', help='Output image path.')
  parser.add_argument('--keep_aspect_ratio', action='store_true',
      help=(
          'keep the image aspect ratio when down-sampling the image by adding '
          'black pixel padding (zeros) on bottom or right. '
          'By default the image is resized and reshaped without cropping. This '
          'option should be the same as what is applied on input images during '
          'model training. Otherwise the accuracy may be affected and the '
          'bounding box of detection result may be stretched.'))
  args = parser.parse_args()
  # Initialize engine.
  engine = DetectionEngine(args.model)
  labels = dataset_utils.read_label_file(args.label) if args.label else None

  with picamera.PiCamera(resolution=(640, 480), framerate=30) as camera:
    camera.start_preview()
    try:
      stream = io.BytesIO()
      for _ in camera.capture_continuous(
          stream, format='jpeg', use_video_port=True):
        stream.seek(0)
        image = Image.open(stream).convert('RGB').resize((640, 480),
                                                         Image.ANTIALIAS)
        start_time = time.time()
        image.save(args.input)
        img = Image.open(args.input).convert('RGB')
        draw = ImageDraw.Draw(img)

        # Run inference.
        objs = engine.detect_with_image(img,
                                        threshold=0.05,
                                        keep_aspect_ratio=args.keep_aspect_ratio,
                                        relative_coord=False,
                                        top_k=10)
        detected_human = 0
        # Print and draw detected objects.
        for obj in objs:
          print('-----------------------------------------')
          if labels:
            print(labels[obj.label_id])
          print('score =', obj.score)
          if obj.score > 0.85:
            detected_human = 1
            
          box = obj.bounding_box.flatten().tolist()
          print('box =', box)
          draw.rectangle(box, outline='red')

        if not objs:
          print('No objects detected.')
          detected_human = 0
        
        file = open(PROJ_DIR + HUMAN_PRESENCE_FILE, 'w')
        file.write('%s' % (str(detected_human)))
        file.close()
          
        # Save image with bounding boxes.
        if args.output:
          img.save(args.output)
        
        elapsed_ms = (time.time() - start_time) * 1000
        stream.seek(0)
        stream.truncate()
    finally:
      camera.stop_preview()


if __name__ == '__main__':
  main()
